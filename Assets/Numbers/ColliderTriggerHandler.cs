﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ColliderTriggerHandler : MonoBehaviour {

    public GameObject effectPrefab;

    // Ebben tároljuk, hogy melyik gameobjectel való collisionhöz milyen gameobjectet hoztunk létre
    private Dictionary<GameObject, GameObject> dict = new Dictionary<GameObject, GameObject>();

    //private void OnCollisionEnter(Collision collision)
    //{
    //    Debug.Log("Enter" + collision.gameObject.name);
    //    var uj = Instantiate(effectPrefab, collision.contacts.First().point, new Quaternion(0,0,0,0));
    //    dict.Add(collision.gameObject, uj);
    //}

    //private void OnCollisionExit(Collision collision)
    //{
    //    Debug.Log("Exit" + collision.gameObject.name);
    //    Destroy(dict.Where(x => x.Key == collision.gameObject).First().Value);
    //}

    private void OnTriggerEnter(Collider other)
    {
        var drawPos = transform.position + new Vector3(-30, 0, 0);
        RaycastHit hit;
        Debug.DrawRay(drawPos, transform.forward * 10000, Color.green, 10000);
        Debug.DrawRay(drawPos, transform.right * 10000, Color.blue, 10000);
        Debug.DrawRay(drawPos, transform.up * 10000, Color.cyan, 10000);

        if (Physics.Raycast(drawPos, transform.right, out hit))
        {
            Debug.Log("Point of contact: " + hit.point);
            var uj = Instantiate(effectPrefab, hit.point, new Quaternion(0, 0, 0, 0));
            dict.Add(other.gameObject, uj);
        }

        Debug.Log("EnterTrigger" + other.gameObject.name);
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("ExitTrigger" + other.gameObject.name);
    }


}
