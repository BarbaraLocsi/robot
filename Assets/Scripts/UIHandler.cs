﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class UIHandler : MonoBehaviour, IUIHandler
{
    public Dictionary<CommandContainer, GameObject> ModelUIDic;
    public List<GameObject> CommandIcons;
    public Button resetButton;
    public Button startButton;

    public void StartCommand(ICommand container, int number)
    {
        ModelUIDic.Where(x => x.Key == (object)container).Select(x => x.Value).FirstOrDefault().transform.GetChild(number).gameObject.GetComponent<Outline>().enabled = true;
    }

    public void EndCommand(ICommand container, int number)
    {
        var command = ModelUIDic.Where(x => x.Key == (object)container).Select(x => x.Value).FirstOrDefault();
        if (command.transform != null &&
            command.transform.childCount > number &&
            command.transform.GetChild(number) != null &&
            command.transform.GetChild(number).gameObject.GetComponent<Outline>() != null)
        {
            command.transform.GetChild(number).gameObject.GetComponent<Outline>().enabled = false;
        }
        
    }

    public void EndAllCommands()
    {
        if (CommandIcons==null)
        {
            fillCommandIcons();
        }

        foreach (var item in CommandIcons)
        {
            if (item != null && item.GetComponent<Outline>()!= null)
            {
                item.GetComponent<Outline>().enabled = false;
            }
        }
    }

    private void fillCommandIcons()
    {
        CommandIcons = new List<GameObject>();
        foreach (var CC_GO_pair in ModelUIDic)
        {
            foreach (Transform child in CC_GO_pair.Value.transform)
            {
                CommandIcons.Add(child.gameObject);
            }
        }
    }
}
