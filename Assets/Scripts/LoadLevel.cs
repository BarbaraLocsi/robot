﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

//TODO: cannot be singleton, because we reference it on buttons in the editor
public class LoadLevel : MonoBehaviour {
    public void LoadScene(string Destination)
    {
        //Application.LoadLevel(Destination);
        SceneManager.LoadScene(Destination);
    }

    public static void LoadNextScene()
    {
        SceneManager.LoadScene(NextScene);
    }

    public void LoadBackScene()
    {
        SceneManager.LoadScene(BackScene);
    }

    public void SetNextScene(string nextScene)
    {
        NextScene = nextScene;
    }

    public void SetBackScene(string backScene)
    {
        BackScene = backScene;
    }

    public void SetSourceFolder(string sourceFolder)
    {
        if (sourceFolder == "standard_maps")
        {
            SourceFolder = Path.Combine(Application.streamingAssetsPath, "standard_maps");
            IsStreaming = true;
        }
        else
        {
            SourceFolder = Path.Combine(Application.persistentDataPath, "custom_maps");
            IsStreaming = false; ;
        }
    }

    private void Start()
    {
        if (SourceFolder == "")
        {
            SetSourceFolder("standard_maps");
        }
    }

    //TODO test
    public static string NextScene = "HardScene";
    public static string BackScene = "MainScene";
    public static string ChosenMap = "";
    public static string SourceFolder = "";
    public static bool IsStreaming;
}
