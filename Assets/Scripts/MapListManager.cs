﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class MapListManager : MonoBehaviour {

    [SerializeField]
    public GameObject listElement;


    /// <summary>
    /// Initializes the ScrollView
    /// listElement has to Button component and a Text child
    /// Sets the Button onClick to open the chosen map
    /// Sets the Text to the name of the map
    /// </summary>
    void Start () {
        
        var fileNames = LoadLevel.IsStreaming ? readMapsStreamingAssets() : readMapsPersistent();

        //DirectoryInfo directoryInfo = new DirectoryInfo(LoadLevel.SourceFolder);
        Debug.Log("LoadLevel.SourceFolder: " + LoadLevel.SourceFolder);
        Debug.Log("Streaming Assets Path: " + Application.streamingAssetsPath);

        foreach (var fileFull in fileNames)
        {
            string fileName = Path.GetFileName(fileFull);
            string fileNameWithoutXML = Path.GetFileNameWithoutExtension(fileFull);

            //string fileName = Path.GetFileName(fileFull.FullName);
            //string fileNameWithoutXML = Path.GetFileNameWithoutExtension(fileFull.Name);
            var newElement = Instantiate(listElement, this.transform);
            newElement.GetComponentInChildren<Text>().text = fileNameWithoutXML;
            newElement.GetComponent<Button>().onClick.AddListener(
                () =>
                {
                    Debug.Log("Chosen map: " + fileName);
                    LoadLevel.ChosenMap = fileName;
                    LoadLevel.LoadNextScene();
                });
        }
    }



    private string[] readMapsStreamingAssets()
    {
        Debug.Log(BetterStreamingAssets.DirectoryExists("standard_maps"));
        return BetterStreamingAssets.GetFiles("standard_maps", "*.xml");

    }

    private string[] readMapsPersistent()
    {
        return Directory.GetFiles(LoadLevel.SourceFolder, "*.xml");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
        
