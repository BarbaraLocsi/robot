﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameChecker : MonoBehaviour {
    public GameObject EndGameUI;

    private int counter;
    private int lightTileFullCount;


    // Use this for initialization
    void Start () {
        counter = 0;
        var tiles = GameObject.FindGameObjectsWithTag("Tile");
        
        foreach (var item in tiles)
        {
            LightTile lightTile = item.GetComponent<LightTile>();

            // If there was a light script attached
            if (lightTile != null)
            {
                // Subsribe to event
                lightTile.lightEvent += LightCounter;
                lightTileFullCount++;
            }
        }
    }

    void Update()
    {
        if (end && endTime + 1 < Time.realtimeSinceStartup)
        {
            EndGameUI.SetActive(true);
        }
    }

    private bool end;
    private float endTime;
    void LightCounter(int n)
    {
        if (n != 1 && n != -1)
            return;

        counter += n;

        if (lightTileFullCount == counter)
        {
            // Map successfully completed
            Debug.Log("WELL DONE");
            //Instantiate(EndGameUI, GameObject.FindGameObjectWithTag("Canvas").transform);
            //EndGameUI.SetActive(true);
            end = true;
            endTime = Time.realtimeSinceStartup;
        }
    }


}
