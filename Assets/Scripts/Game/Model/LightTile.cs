﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public delegate void LightEvent(int n);

public class LightTile : MonoBehaviour {
    public Material OffMaterial;
    public Material OnMaterial;
    private Light spotLight;
    public LightEvent lightEvent;

    [SerializeField] private SpriteRenderer imageSpot;

    private Renderer renderer;

    private bool _isOn;
    private bool IsOn
    {
        get
        {
            return _isOn;
        }
        set
        {
            if (_isOn != value)
            {
                if (lightEvent != null)
                {
                    lightEvent(value == true ? 1 : -1);
                }
            }
            _isOn = value;
        }
    }


    void Start () {
        IsOn = false;
        //spotLight = transform.GetChild(0).GetComponent<Light>();
        renderer = GetComponent<Renderer>();
        turnOff();
    }

    public void Light() {
        if (IsOn)
        {
            turnOff();
        }
        else {
            turnOn();
        }
    }

    public void ResetLight()
    {
        turnOff();
    }

    private void turnOn()
    {
        //spotLight.enabled = true;
        //imageSpot.enabled = true;
        renderer.material = OnMaterial;
        IsOn = true;
    }

    private void turnOff()
    {
        //spotLight.enabled = false;
        //imageSpot.enabled = false;
        renderer.material = OffMaterial;
        IsOn = false;
    }

}
