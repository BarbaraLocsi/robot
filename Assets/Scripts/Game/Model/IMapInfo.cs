﻿using UnityEngine;

internal interface IMapInfo
{
    bool canGoHere(Vector3 coordinates);
    bool jumpHere(Vector3 coordinates);
    float? getJumpYCoordinate(float x, float z, Bounds robotBounds, Vector3 from);
}