﻿using System;
using System.Collections;
using UnityEngine;

public class TurnRightCommand : MonoBehaviour, ICommand
{
    private Robot p;

    public void SetPlayer()
    {
        GameObject robotGO = GameObject.FindGameObjectWithTag("Robot");
        p = robotGO.GetComponent<Robot>();
    }

    public IEnumerator Execute()
    {
        if (p == null)
        {
            SetPlayer();
        }

        yield return StartCoroutine(p.TurnRight());
        Debug.Log("TurnRightCommand");
    }
}