﻿using System;
using System.Collections;
using UnityEngine;

public class TurnLeftCommand : MonoBehaviour, ICommand
{

    private Robot p;

    public void SetPlayer()
    {
        GameObject[] robots = GameObject.FindGameObjectsWithTag("Robot");
        GameObject robotGameObject = robots[0];
        p = robotGameObject.GetComponent<Robot>();
    }

    public IEnumerator Execute()
    {
        if (p == null)
        {
            SetPlayer();
        }

        yield return StartCoroutine(p.TurnLeft());
        Debug.Log("TurnLeftCommand");
    }
}