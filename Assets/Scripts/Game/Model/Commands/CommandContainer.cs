﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CommandContainer : MonoBehaviour, ICommand
{
    public static bool Stop;

    private List<ICommand> commands;

    void Awake() {
        commands = new List<ICommand>();
        Stop = false;
    }

    public void AddCommand(ICommand c) {
        commands.Add(c);
    }

    public IEnumerator Execute()
    {
        var uiGO = GameObject.FindGameObjectWithTag("UI");
        IUIHandler ui = null;
        if (uiGO != null)
        {
            ui = uiGO.GetComponent<IUIHandler>();
        }

        Debug.Log("CommandContainer");
        int i = 0;
        foreach (var c in commands)
        {
            if (Stop == true)
            {
                //commands.Clear();
                yield break;
            }

            uIStartCommand(ui, i);
            Debug.Log("Start Corutine " + c.ToString());
            yield return StartCoroutine(c.Execute());
            uIEndCommand(ui, i++);
        }
    }

    private void uIStartCommand(IUIHandler ui, int i)
    {
        if (ui!=null)
        {
            ui.StartCommand(this, i);
        }
    }

    private void uIEndCommand(IUIHandler ui, int i)
    {
        if (ui != null)
        {
            ui.EndCommand(this, i);
        }
    }

    internal void RemoveAll()
    {
        //commands.Clear();
        commands.RemoveAll(x => true);
    }
}

