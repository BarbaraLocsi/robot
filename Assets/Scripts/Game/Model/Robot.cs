﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Robot : MonoBehaviour {

    public float speed;
    public float waitTime = 0.5f;
    private Rigidbody rb;
    private Vector3 originalPosition;
    private Quaternion originalRotation;

    private GameObject originalPositionGO;

    private bool _isMoving;

    public bool isMoving
    {
        get
        {
            return _isMoving;
        }
        set
        {
            _isMoving = value;
            Debug.Log("isMoving: " + value);
            Debug.Log(Environment.StackTrace);
        }
    }



    void Start()
    {
        
        rb = GetComponent<Rigidbody>();
        originalPosition = transform.position;
        originalRotation = transform.rotation;
        isMoving = false;


        var creator = GameObject.FindGameObjectWithTag("Map").GetComponent<Creator>();
        originalPositionGO = creator.originalPositionGO;
    }

    /// <summary>
    /// Checks if the robot can go here
    /// </summary>
    /// <returns></returns>
    private bool canGoHere(Vector3 targetPosition)
    {
        GameObject gameObjectWithMapTag = GameObject.FindGameObjectsWithTag("Map")[0];
        IMapInfo mapInfo = gameObjectWithMapTag.GetComponent(typeof(IMapInfo)) as IMapInfo;
        // the bottom of the robot
        var bounds = GetComponent<Collider>().bounds;

        Debug.Log("bounds"+bounds);

        Vector3 bottomOfRobot = bounds.min;
        Debug.Log("target" + targetPosition);
        Debug.Log("bottom of robot" + bottomOfRobot );

        var result = new Vector3(targetPosition.x, targetPosition.y - bounds.extents.y, targetPosition.z);

        Debug.Log("result" + result);

        return mapInfo.canGoHere(targetPosition);
    }

    #region Jump
    public IEnumerator Jump()
    {
        isMoving = true;
        Vector3 targetPosition = getJumpTargetPosition();

        if (!canGoHere(targetPosition))
        {
            Debug.Log("Can't jump there");
            yield return StartCoroutine(waitAfterMoving());
            isMoving = false;
            yield break;
        }

        // Ha nem okozott magasság változást akkor egyhelyben ugrottunk 
        if (Math.Abs(targetPosition.y - transform.position.y) < 0.01f)
        { 
            targetPosition = transform.position;
        }


        float jumpTime = setJumpVelocity(targetPosition);

        float waitSum = 0;

        waitSum += jumpTime;

        yield return new WaitForSeconds(jumpTime);

        var vOrigo = new Vector3(0.0f, 0.0f, 0.0f);
        while (Vector3.Distance(transform.position, targetPosition) > 0.002f)
        {
            if (Vector3.Distance(rb.velocity, vOrigo) < 0.001f)
            {
                break;
            }
            
            waitSum += 0.03f;
            yield return new WaitForSeconds(0.03f);
        }
        
        transform.position = targetPosition;

        rb.velocity = new Vector3(0.0f, 0.0f, 0.0f);
        if (waitSum < waitTime)
        {
            yield return new WaitForSeconds(waitTime - waitSum);
        }

        isMoving = false;
    }

    private Vector3 getJumpTargetPosition()
    {
        GameObject gameObjectWithMapTag = GameObject.FindGameObjectsWithTag("Map")[0];
        IMapInfo mapInfo = gameObjectWithMapTag.GetComponent(typeof(IMapInfo)) as IMapInfo;


        // Az ugrás érkezési helyének pontos x és z koordinátái ( y-t késöbb számoljuk)
        Vector3 temp = transform.position + transform.forward;

        // Az ugrás érkezési helyének y koordinátájának kiszámolása
        float? ycoord = mapInfo.getJumpYCoordinate(temp.x, temp.z, GetComponent<Collider>().bounds, transform.position);
        if (ycoord == null)
        {
            // Ha nem sikerült az y koordináta kiszámítás, akkor nem lehet ebben az irányban ugrani, így egy helyben ugrunk
            return transform.position;
        }

        // Az ugrás érkezési helye, mind a 3 koordináta jó
        Vector3 targetPosition = new Vector3(temp.x, ycoord.Value, temp.z);
        targetPosition = new Vector3(Mathf.Round(targetPosition.x * 4.0f) / 4, Mathf.Round(targetPosition.y * 4.0f) / 4, Mathf.Round(targetPosition.z * 4.0f) / 4);
        return targetPosition;
    }

    /// <summary>
    /// Beállítja a Robot sebességét
    /// </summary>
    /// <param name="targetPosition">Az ugrás célja</param>
    /// <returns>Az ugráshoz szükséges idő</returns>
    private float setJumpVelocity(Vector3 targetPosition)
    {
        float gravity = Physics.gravity.magnitude;

        // Ha nem okozna magasság változást akkor egy helyben ugrunk
        if (Math.Abs(targetPosition.y-transform.position.y) < 0.001f)
        {
            float vy = 5;
            rb.velocity = new Vector3(0, vy, 0);
            return vy / gravity * 2;
        }

        // Az ugrás szöge
        float alpha = 70 * Mathf.Deg2Rad;
        
        // Magasság különbség
        float yOffset = transform.position.y - targetPosition.y;

        // XZ síkban a távolság
        Vector3 targetXZ = new Vector3(targetPosition.x, 0, targetPosition.z);
        Vector3 actualXZ = new Vector3(transform.position.x, 0, transform.position.z);
        float distanceXZ = Vector3.Distance(targetXZ, actualXZ);

        float v0 = (1 / Mathf.Cos(alpha)) *
                   Mathf.Sqrt(
                     (0.5f * gravity * distanceXZ * distanceXZ) /
                     (Mathf.Tan(alpha) * distanceXZ + yOffset)
                   );

        Vector3 velocity = new Vector3(0, v0 * Mathf.Sin(alpha), v0 * Mathf.Cos(alpha));

        // Sebesség vektor elforgatása
        float angleBetweenObjects = Vector3.Angle(Vector3.forward, targetXZ - actualXZ) * (targetPosition.x > transform.position.x ? 1 : -1);
        Vector3 finalVelocity = Quaternion.AngleAxis(angleBetweenObjects, Vector3.up) * velocity;

        // Robot sebesség beállítása
        rb.velocity = finalVelocity;
        // Kiszámoljuk  mennyi idő alatt esik le
        float jumpTime = distanceXZ / (v0 * Mathf.Cos(alpha));

        return jumpTime;
    }
    #endregion


    #region GoForward

    public IEnumerator GoForward()
    {
        isMoving = true;
        Vector3 targetPosition = transform.position + transform.forward;
        if (!canGoHere(targetPosition))
        {
            Debug.Log("Can't go there");
            yield return StartCoroutine(waitAfterMoving());
            isMoving = false;
            yield break;
        }
        float waitSum = 0;
        while (Vector3.Distance(transform.position, targetPosition) > 0.01f)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, Time.deltaTime * speed);
            Debug.Log("movingtovards" + targetPosition);
            waitSum += 0.03f;
            yield return new WaitForSeconds(0.03f);
        }
        transform.position = targetPosition;
        if (waitSum < waitTime) {
               yield return new WaitForSeconds(waitTime-waitSum);
        }

        isMoving = false;
    }
    #endregion

    #region Turn
    public IEnumerator TurnLeft()
    {
        transform.Rotate(0, -90, 0);
        yield return StartCoroutine(waitAfterMoving());
    }


    public IEnumerator TurnRight()
    {
        transform.Rotate(0, 90, 0);
        yield return StartCoroutine(waitAfterMoving());
    }
    #endregion

    #region Light
    public IEnumerator Light()
    {
        Debug.Log("RobotLight");
        // Declare a raycast hit to store information about what our raycast has hit
        RaycastHit hit;
        var rayCastStartPosition = transform.position + new Vector3(0, 10, 0);
        var rayCastDirection = -transform.up;
        Debug.DrawRay(rayCastStartPosition, rayCastDirection, Color.green, 100.0f);
        // Check if our raycast has hit anything

        RaycastHit[] hits;
        hits = Physics.RaycastAll(rayCastStartPosition, rayCastDirection, 10000);
        for (int i = 0; i < hits.Length; i++)
        {
            // Get a reference to a light script attached to the collider we hit
            LightTile lightTile = hits[i].collider.GetComponent<LightTile>();

            // If there was a light script attached
            if (lightTile != null)
            {
                // Call the light function of that script
                lightTile.Light();
            }
        }
        
        yield return StartCoroutine(waitAfterMoving());
    }
    #endregion

    private IEnumerator waitAfterMoving()
    {
        yield return new WaitForSeconds(waitTime);
    }

    #region Reset
    public void ResetOriginalPosition()
    {
        transform.position = originalPosition + new Vector3(0, 2, 0);
        transform.rotation = originalRotation;
    }

    public void ResetOriginalPositionValueToOriginalPositionGO()
    {
        originalPosition = originalPositionGO.transform.position;
        originalRotation = originalPositionGO.transform.rotation;
    }

    #endregion
}
