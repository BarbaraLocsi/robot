﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Model
{
    class MapInfo : MonoBehaviour, IMapInfo
    {
        public bool canGoHere(Vector3 coordinates)
        {
            GameObject[] tiles = GameObject.FindGameObjectsWithTag("Tile");

            // Megnézzük, hogy van-e tile az adott pozición
            foreach (var tile in tiles)
            {
                if (tile.GetComponent<Collider>().bounds.Contains(coordinates+new Vector3(0,0.1f,0)))
                {
                    Debug.Log(tile.GetComponent<Collider>().bounds);
                    return false;
                }
            }

            // Megnézzük, hogy van-e az új pozíció alatt tile
            RaycastHit hit;
            if (Physics.Raycast(coordinates + new Vector3(0, 0.1f, 0), Vector3.down, out hit, 0.2f))
            {
                // Ha van alatta valami megnézzük, hogy tile-e az
                if (hit.transform.gameObject.CompareTag("Tile"))
                {
                    return true;
                }
            }
            return false;
        }

        public bool jumpHere(Vector3 coordinates)
        {
            throw new NotImplementedException();
        }

        public float? getJumpYCoordinate(float x, float z, Bounds robotBounds, Vector3 from)
        {
            /*
            float eps = 0.01f;
            var tiles = GameObject.FindGameObjectsWithTag("Tile").ToList();

            float max = float.MinValue;
            bool ok = false;
            foreach (var tile in tiles)
            {
                if (Mathf.Abs(tile.transform.position.x - x) < eps && Mathf.Abs(tile.transform.position.z - z) < eps)
                {
                    if (max < tile.transform.position.y)
                    {
                        max = tile.transform.position.y;
                        ok = true;
                    }
                }
            }
            */
            float tileHight = 0.5f;

            var y = from.y+ tileHight + 0.01f;
            // Megnézzük, hogy van-e az új pozíció alatt tile
            RaycastHit hit;
            //Debug.DrawRay(new Vector3(x, y, z), Vector3.down, Color.green,100);
            if (Physics.Raycast(new Vector3(x, y, z), Vector3.down, out hit))
            {
                // Ha van alatta valami megnézzük, hogy tile-e az
                if (hit.transform.gameObject.CompareTag("Tile"))
                {
                    return hit.transform.gameObject.transform.position.y + tileHight/2;//robotBounds.center.y;//0.75f;
                }
            }
            return null;

            /*
            if (!ok) return null;

            // a tilenak a közepe volt a koordinatarendszere
            return max + 0.75f;*/
        }
    }
}
