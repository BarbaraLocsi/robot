﻿interface IUIHandler
{
    void StartCommand(ICommand container, int number);
    void EndCommand(ICommand container, int number);
    void EndAllCommands();
}