﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class Creator : MonoBehaviour {

    public int levelNumber;
    public GameObject tile;
    public GameObject lightTile;
    public GameObject player;
    public float tileX;
    public float tileY;

    [SerializeField]
    private GameObject parentOfTiles;


    private float tileZ;


    private Vector3 position;
    private Vector3 positionX2;
    private Vector3 positionY2;
    private Vector3 positionZ2;
    private Quaternion rotation;

    public GameObject originalPositionGO { get; private set; }


    void Awake() {
        tileZ = tileX;
        position = new Vector3(0.0f, 0.0f, 0.0f);
        positionX2 = new Vector3(tileX, 0.0f, 0.0f);
        positionY2 = new Vector3(0.0f, tileY, 0.0f);
        positionZ2 = new Vector3(0.0f, 0.0f, tileZ);
        rotation = new Quaternion();
        if (levelNumber == 2)
        {
            loaded();
        }
    }

    void loaded(){
        var mapEntry = XMLManager.Instance.Load(LoadLevel.ChosenMap);
        LoadLevel.ChosenMap = "";
        openSavedMap(mapEntry);
    }

    public void openSavedMap(MapEntry mapEntry)
    {
        List<TileEntry> tileList = mapEntry.tileList;


        double zMax = tileList.Max(x => x.point.z);
        double xMax = tileList.Max(x => x.point.x);
        double zMin = tileList.Min(x => x.point.z);
        double xMin = tileList.Min(x => x.point.x);

        // Calculate average for camera centering
        int zAvg = (int)(zMin + (zMax - zMin) / 2);
        int xAvg = (int)(xMin + (xMax - xMin) / 2);
        int yCenter = 1;

        //Set camera to center
        Vector3 centerPosition = position + (xAvg * positionX2) + (yCenter * positionY2) + (zAvg * positionZ2);
        Debug.Log("Camera center to:" + centerPosition);

        if (Camera.main!=null)
        {
            var cameraCenterComponent = Camera.main.GetComponent<CameraCenter>();
            if (cameraCenterComponent != null)
            {
                cameraCenterComponent.Center(centerPosition);
            }
        }

        Debug.Log("z: " + zAvg);
        Debug.Log("x: " + xAvg);
        foreach (var tile in tileList)
        {
            if (tile.type != TileTypeEnum.StartPosition)
            {
                var tileGameObject = Instantiate(getPrefab(tile.type), position + (tile.point.x * positionX2) + (tile.point.y * positionY2) + (tile.point.z * positionZ2), rotation);

                if (parentOfTiles != null)
                {
                    tileGameObject.transform.parent = parentOfTiles.transform;
                }
            }
            else
            {
                // The tile is the start position indicator
                var p = Instantiate(player, position + (tile.point.x * positionX2) + ((tile.point.y + 1) * positionY2) + (tile.point.z * positionZ2), rotation);
                //p.transform.Rotate(0, 90, 0);

                originalPositionGO = new GameObject("originalPositionGO");
                originalPositionGO.transform.position = p.transform.position;
                originalPositionGO.transform.rotation = p.transform.rotation;

                if (parentOfTiles != null)
                {
                    p.transform.parent = parentOfTiles.transform;
                    originalPositionGO.transform.parent = parentOfTiles.transform;
                }
            }


        }
        //TODO
        if (parentOfTiles != null)
        {
            parentOfTiles.transform.position = parentOfTiles.transform.position + new Vector3(4f, 0, 4f);
            //parentOfTiles.transform.localScale = parentOfTiles.transform.localScale * 1.25f;
        }
    }

    public GameObject getPrefab(TileTypeEnum type)
    {
        switch (type)
        {
            case TileTypeEnum.LightTile: return lightTile;
            case TileTypeEnum.Tile: return tile;
            //case TileTypeEnum.StartPosition: return StartPosition;
            default: return tile;
        }
    }


    public void ResetMap()
    {
        //TODO: creator should keep a list of the tiles
        var tiles = GameObject.FindGameObjectsWithTag("Tile");
        //var lightTiles = tiles.ToList().ForEach(x=>x.GetComponent<LightTile>().Reset());

        foreach (var lightTile in tiles)
        {
            var component = lightTile.GetComponent<LightTile>();
            if (lightTile.GetComponent<LightTile>() != null)
            {
                component.ResetLight();
            }
        }
    }


}
