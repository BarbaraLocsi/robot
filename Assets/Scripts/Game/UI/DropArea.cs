﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropArea : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    public int maxSize;
    public bool isInfinitStore = false;
    private int currentSize;

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log(eventData.pointerDrag.name + "was draged on to: " + gameObject.name);

        if (transform.childCount < maxSize)
        {
            // Mit dragelünk
            Dragable d = eventData.pointerDrag.GetComponent<Dragable>();
            if (d != null)
            {
                d.parentToReturnTo = this.transform;
                currentSize++;
            }
        }
        else
        {
            Debug.Log("Megtelt az area");
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
    }

    public void OnPointerExit(PointerEventData eventData)
    {
    }

    // Use this for initialization
    void Start()
    {
        currentSize = 0;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
