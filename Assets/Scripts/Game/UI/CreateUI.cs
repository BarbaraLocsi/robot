﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateUI : MonoBehaviour {

    //public int NumberOfExtraContainers;

    /// <summary>
    /// First is main
    /// </summary>
    public int[] ContainerCapacitys;
    public GameObject ContainerPrefab;
    public GameObject FunctionButtonPrefab;

    public GameObject ForwardButton;
    public GameObject RightButton;
    public GameObject LeftButton;
    public GameObject JumpButton;
    public GameObject LightButton;

    void Start () {
        GameObject commandPanel = GameObject.FindGameObjectWithTag("CommandPanel");

        GameObject commandContainerPanel = GameObject.FindGameObjectWithTag("CommandContainerPanel");
        bool main = true;
        int i = 1;
        foreach (var capacity in ContainerCapacitys)
        {
            GameObject g = Instantiate(ContainerPrefab, commandContainerPanel.transform);
            g.transform.GetComponent<DropArea>().maxSize = capacity;

            if (main)
            {
                g.name = "MainCommandContainer";
            }
            else
            {
                g.name = "ProcContainer " + i;
                // Készítünk hozzá gombot
                
                GameObject functionCommand = Instantiate(FunctionButtonPrefab, commandPanel.transform);
                functionCommand.name = "ProcContainer " + i;
                functionCommand.GetComponentInChildren<Text>().text = "P" + i;

                i++;
            }
            main = false;
        }
	}
}
