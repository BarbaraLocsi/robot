﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public class StartButtonClick : MonoBehaviour
{
    public Button resetButton;
    public Button startButton;
    public GameObject CommandContainerScript;

    public GameObject Scripts;


    void Start()
    {
        startButton.onClick.AddListener(DoRealCommands);
    }

    private Dictionary<string, CommandContainer> ModelCCDic;
    private Dictionary<CommandContainer, GameObject> ModelUIDic;
    private List<GameObject> CommandIcons;

    private void DoRealCommands()
    {
        CommandIcons = new List<GameObject>();
        CommandContainer.Stop = false;

        var modelGameObject = GameObject.FindGameObjectWithTag("Model");
        if (modelGameObject != null)
        {
            GameObject.Destroy(modelGameObject);
        }

        modelGameObject = new GameObject();
        modelGameObject.name = "Model";
        modelGameObject.tag = "Model";

        GameObject[] containers = GameObject.FindGameObjectsWithTag("CommandContainer");

        ModelCCDic = new Dictionary<string, CommandContainer>();
        ModelUIDic = new Dictionary<CommandContainer, GameObject>();

        foreach (var container in containers)
        {
            // Minden containerhez csinálunk egy modelbeli containert
            GameObject ccs = Instantiate(CommandContainerScript, modelGameObject.transform);
            CommandContainer cc = ccs.GetComponent<CommandContainer>();
            ModelCCDic.Add(container.transform.name, cc);
            ModelUIDic.Add(cc, container);
        }

        CommandContainer main = null;
        foreach (var container in containers)
        {
            Transform containerTransform = container.transform;

            CommandContainer cc = ModelCCDic.FirstOrDefault(x => x.Key == containerTransform.name).Value;


            if (containerTransform.name == "MainCommandContainer")
            {
                main = cc;
            }

            // Reseteljük a containereket
            cc.RemoveAll();

            for (int i = 0; i < containerTransform.childCount; i++)
            {
                ICommand ic = tagSwitcher(containerTransform.GetChild(i).tag, containerTransform.GetChild(i).name, modelGameObject.transform);
                cc.AddCommand(ic);
                CommandIcons.Add(containerTransform.GetChild(i).gameObject);
            }
        }

        // Átadjuk a UIHandlernek a gombokat
        UIHandler ui = GameObject.FindGameObjectWithTag("UI").GetComponent<UIHandler>();
        ui.ModelUIDic = ModelUIDic;
        ui.CommandIcons = CommandIcons;

        StartCoroutine(main.Execute());

        // Reset buttont bekapcsoljuk, Start button kikapcsoljuk
        resetButton.gameObject.SetActive(true);
        startButton.gameObject.SetActive(false);

        resetButton.enabled = true;
        startButton.enabled = false;

    }

    private ICommand tagSwitcher(string tag, string name, Transform parent)
    {
        GameObject scriptsGO = Instantiate(Scripts, parent);
        ICommand command = null;
        switch (tag)
        {
            case "Jump":
                command = scriptsGO.GetComponent<JumpCommand>();
                break;
            case "Forward":
                command = scriptsGO.GetComponent<ForwardCommand>();
                break;
            case "Left":
                command = scriptsGO.GetComponent<TurnLeftCommand>();
                break;
            case "Right":
                command = scriptsGO.GetComponent<TurnRightCommand>();
                break;
            case "Light":
                command = scriptsGO.GetComponent<LightCommand>();
                break;
            case "Proc":
                if (ModelCCDic.Any(x => name.Contains(x.Key)))
                {
                    command = ModelCCDic.First(x => name.Contains(x.Key)).Value;
                }
                break;
            default:
                break;
        }
        return command;
    }


}
