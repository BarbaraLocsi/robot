﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Dragable : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler {

    public Transform parentToReturnTo = null;
    


    public void OnBeginDrag(PointerEventData eventData)
    {
        // Ha drop area akkor nem infinit
        bool isParentInfinitStore = transform.parent.GetComponent<DropArea>() == null ? true : false ;

        if (isParentInfinitStore)
        {
            int siblingIndex = this.transform.GetSiblingIndex();
            Dragable newGameObject = Instantiate(this, transform.parent);
            newGameObject.transform.SetSiblingIndex(siblingIndex);
            transform.SetParent(this.transform.parent.parent.parent);
            parentToReturnTo = null;
        }
        else
        {
            //parentToReturnTo = transform.parent;
            parentToReturnTo = null;
            //3 parent mert a canvasra szeretném rakni
            Debug.Log(this.transform.parent.parent.parent.name);
            transform.SetParent(this.transform.parent.parent.parent);
            transform.position = eventData.position;
        }

        

        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (parentToReturnTo == null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            transform.SetParent(parentToReturnTo);
        }

        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
