﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResetButton : MonoBehaviour {

    public Button startButton;
    public Button resetButton;
    private Robot robot;
    private Creator creator;

    void Awake()
    {
        resetButton.onClick.AddListener(onButtonClick);
        
    }

    void Start()
    {
        resetButton.gameObject.SetActive(false);
    }

    private void onButtonClick()
    {
        resetButton.enabled = false;
        startButton.enabled = false;
        Debug.Log("ResetStart");
        if (robot == null)
        {
            robot = GameObject.FindGameObjectWithTag("Robot").GetComponent<Robot>();
        }
        if (creator == null)
        {
            creator = GameObject.FindGameObjectWithTag("Map").GetComponent<Creator>();
        }
        IUIHandler ui = GameObject.FindGameObjectWithTag("UI").GetComponent<IUIHandler>();
        CommandContainer.Stop = true;
        ui.EndAllCommands();
        creator.ResetMap();
        StartCoroutine(ResetRobot());
        //robot.ResetOriginalPosition();

        startButton.gameObject.SetActive(true);
        resetButton.gameObject.SetActive(false);
        Debug.Log("ResetEnd");
    }

    private IEnumerator ResetRobot()
    {
        while (robot.isMoving)
        {
            yield return new WaitForSeconds(0.03f);
        }
        robot.ResetOriginalPosition();
        Debug.Log("ResetCorutineEnd");

        startButton.enabled = true;
    }

}
