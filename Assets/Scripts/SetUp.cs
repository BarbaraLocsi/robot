﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SetUp : MonoBehaviour {

	// Use this for initialization
	void Start () {
        BetterStreamingAssets.Initialize();
        //System.IO.Directory.CreateDirectory(Path.Combine(Application.dataPath, "standard_maps"));
        System.IO.Directory.CreateDirectory(Path.Combine(Application.persistentDataPath, "custom_maps"));
        System.IO.Directory.CreateDirectory(Path.Combine(Application.persistentDataPath, "saved_images"));
    }
}
