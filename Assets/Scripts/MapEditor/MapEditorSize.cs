﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapEditorSize : MonoBehaviour {

    /// <summary>
    /// The change size panel
    /// </summary>
    [SerializeField] Button sizePanelButton;
    [SerializeField] GameObject mapSizePanel;

    /// <summary>
    /// The save button on the mapSizePanel
    /// </summary>
    [SerializeField] Button sizeSaveButton;
    [SerializeField] Text inputX;
    [SerializeField] Text inputZ;

    private void Start()
    {
        sizePanelButton.onClick.AddListener(
            () =>
            {
                mapSizePanel.SetActive(!mapSizePanel.activeSelf);
            });

        sizeSaveButton.onClick.AddListener(
            () => 
            {
                mapSizePanel.SetActive(false);

                try
                {
                    SizeX = int.Parse(inputX.text);
                    SizeZ = int.Parse(inputZ.text);
                }
                catch (System.Exception)
                {
                    
                    GameObject.FindObjectOfType<WarningMessageHandler>().SetWarningMessage("Use numbers to set map size!");
                    return;
                }

                if (SizeX <= 0 || SizeZ <= 0)
                {
                    GameObject.FindObjectOfType<WarningMessageHandler>().SetWarningMessage("Be positive!");
                    return;
                }
                GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().FieldSizeChanged(SizeX, SizeZ);
            });

    }

    public int SizeX = 5;
    public int SizeZ = 5;
}
