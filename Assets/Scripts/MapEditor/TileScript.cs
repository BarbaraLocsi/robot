﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[System.Serializable]
public class TileScript : MonoBehaviour {

    public Point MapPosition { get; set; }

    [SerializeField]
    private bool canBeDestroyed;

    public bool CanBeDestroyed
    {
        get
        {
            return canBeDestroyed;
        }
    }

    public TileTypeEnum type;

    // Use this for initialization
    void Start () {
        //GameManager.Instance.addTileToList(this);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Setup(Point mapPosition)
    {
        this.MapPosition = mapPosition;
    }

    private void OnMouseOver()
    {

        var gameControllerObj = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();

        // No need for clicked button for left click delete
        if (Input.GetMouseButtonDown(1))
        {
            RemoveTile();
        }

        var clickedButton = gameControllerObj.ClickedButton;
        if (clickedButton == null)
        {
            return;
        }
        if (Input.GetMouseButtonDown(0) && clickedButton.IsDeleteButton == false)
        {
            placeTile();
        }
        else if ((Input.GetMouseButtonDown(0) && clickedButton.IsDeleteButton == true))
        {
            RemoveTile();
        }
    }

    private void placeTile()
    {
        var gameControllerObj = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();

        // If start position tile is selected we delete the previous start position tile
        if (gameControllerObj.ClickedButton.ObjectPrefab.GetComponent<TileScript>().type == TileTypeEnum.StartPosition)
        {
            var startPositionGO = GameObject.FindGameObjectWithTag("StartPosition");
            if (startPositionGO != null)
            {
                var script = startPositionGO.GetComponent<TileScript>();

                if (script != this)
                {
                    if (!EventSystem.current.IsPointerOverGameObject())
                    {
                        Debug.Log("!EventSystem.current.IsPointerOverGameObject()");
                        script.RemoveTile();
                    }
                }
                else
                {
                    Debug.Log("egyenlo");
                    return;
                }
            }
        }

        Point newPosition = new Point(this.MapPosition.x, this.MapPosition.y + 1, this.MapPosition.z);
        // Csak akkor rakunk új tilet ha nincs még ebben a pozicióban
        if (!gameControllerObj.isTileAt(newPosition))
        {
            var clickedButton = gameControllerObj.ClickedButton;
            if (!EventSystem.current.IsPointerOverGameObject() && clickedButton != null)
            {
                TileScript tileScript = Instantiate(clickedButton.ObjectPrefab, transform.position + new Vector3(0, 0.5f, 0), Quaternion.identity).GetComponent<TileScript>(); ;
                tileScript.Setup(newPosition);
                gameControllerObj.addTileToList(tileScript);
            }
        }
    }

    public void placeTileTEST(TileTypeEnum tileType)
    {
        var gameControllerObj = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        var tileTypePrefab = GameObject.FindGameObjectWithTag("TileTypeEnumToPrefab").GetComponent<TileTypeToPrefab>();

        Point newPosition = new Point(this.MapPosition.x, this.MapPosition.y + 1, this.MapPosition.z);
        // Csak akkor rakunk új tilet ha nincs még ebben a pozicióban
        if (!gameControllerObj.isTileAt(newPosition))
        {
                TileScript tileScript = Instantiate(tileTypePrefab.getPrefab(tileType), transform.position + new Vector3(0, 0.5f, 0), Quaternion.identity).GetComponent<TileScript>(); ;
                tileScript.Setup(newPosition);
                gameControllerObj.addTileToList(tileScript);
        }
    }

    public void RemoveTile()
    {
        //TODO: get component and save it so dont have to get it in every function.
        var gameControllerObj = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();

        if (canBeDestroyed)
        {
            gameControllerObj.removeTile(this);
            Destroy(this.gameObject);
        }
    }
}
