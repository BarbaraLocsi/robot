﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectButton : MonoBehaviour {




    [SerializeField]
    private GameObject objectPrefab;

    public GameObject ObjectPrefab
    {
        get { return objectPrefab; }
    }


    [SerializeField]
    private bool isDeleteButton;

    public bool IsDeleteButton
    {
        get
        {
            return isDeleteButton;
        }
    }

    [SerializeField]
    private Image panelImage;

    public void Select()
    {
        panelImage.enabled = true;
    }

    public void UnSelect()
    {
        panelImage.enabled = false;

    }
}
