﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System;

/// <summary>
/// Handles XML operations: saving and loading map.
/// </summary>
public class XMLManager : Singleton<XMLManager> {

    // Save image Source: E.g.: Icon made by Freepik from www.flaticon.com 
    /// <summary>
    /// Saves the map to Application.persistentDataPath
    /// </summary>
    public void Save()
    {
        var mapEditorSizeGO = GameObject.FindObjectOfType<MapEditorSize>();
        var gameControllerObj = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();

        XmlSerializer xmlSerializer = new XmlSerializer(typeof(MapEntry));

        //TODO: Change name
        var date = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");

        string folderPath = Path.Combine(Application.persistentDataPath, "custom_maps");
        string fileName = "map_data_" + date + ".xml";
        string fullPath = Path.Combine(folderPath, fileName);

        var encoding = System.Text.Encoding.GetEncoding("UTF-8");

        StreamWriter stream = new StreamWriter(fullPath, false, encoding);

        MapEntry mapEntry = new MapEntry()
        {
            creationDate = DateTime.Now,
            mapName = "Name",
            tileList = gameControllerObj.GetMapData(),
            xSize = mapEditorSizeGO.SizeX,
            ySize = mapEditorSizeGO.SizeZ,
        };

        xmlSerializer.Serialize(stream, mapEntry);

        stream.Close();
    }

    /// <summary>
    /// Opens the chosen map
    /// </summary>
    /// <param name="mapName"></param>
    /// <returns></returns>
    public MapEntry Load(string mapName)
    {
        var sourceStreamingAssets = "standard_maps/" + mapName;
        var source = Path.Combine(LoadLevel.SourceFolder, mapName);


        XmlSerializer xmlSerializer = new XmlSerializer(typeof(MapEntry));
        Stream stream = LoadLevel.IsStreaming ?
                            BetterStreamingAssets.OpenRead(sourceStreamingAssets) :
                            new FileStream(source, FileMode.Open);

        var map = xmlSerializer.Deserialize(stream) as MapEntry;

        stream.Close();
        return map;
    }

}
