﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GameManager : MonoBehaviour
{
    List<TileScript> tileList;

    public ObjectButton ClickedButton { get; private set; }

    // Use this for initialization
    void Awake () {
        tileList = new List<TileScript>();
    }

    void Start()
    {
        if (LoadLevel.ChosenMap != "")
        {
            LoadMap(LoadLevel.ChosenMap);
            LoadLevel.ChosenMap = "";
        }
        else
        {
            // If no map was chosen to edit, then we make an empty 5x5 field
            GameObject.FindObjectOfType<FieldCreator>().createField(5, 5);
        }
    }

    private void LoadMap(string mapPath)
    {
        // Have to make a correct size field
        var mapData = XMLManager.Instance.Load(LoadLevel.ChosenMap);

        var MapEditor = GameObject.FindObjectOfType<MapEditorSize>();
        MapEditor.SizeX = mapData.xSize;
        MapEditor.SizeZ = mapData.ySize;


        GameObject.FindObjectOfType<FieldCreator>().createField(mapData.ySize, mapData.xSize);

        // Have to load a map
        ReloadMap(mapData.tileList);
    }

    public void FieldSizeChanged(int SizeX, int SizeZ)
    {
        var notField = tileList.Where(x => x.type != TileTypeEnum.FieldTile).ToList();

        if (notField.Count ==0 ||
            (notField.Max(x=>x.MapPosition.x) < SizeX &&
             notField.Max(x => x.MapPosition.z) < SizeZ))
        {
            GameObject.FindObjectOfType<FieldCreator>().removeField();
            GameObject.FindObjectOfType<FieldCreator>().createField(SizeZ, SizeX);
        }
        else
        {
            Debug.Log("First delete tiles on the side");
            GameObject.FindObjectOfType<WarningMessageHandler>().SetWarningMessage("Delete tiles before changing map size!");
        }

    }

    public void addTileToList(TileScript newTile)
    {
        tileList.Add(newTile);
    }

    public bool isTileAt(Point position)
    {
        foreach (var tile in tileList)
        {
            if (position == tile.MapPosition)
            {
                return true;
            }
        }
        return false;
    }

    public void removeTile(TileScript tile)
    {
        tileList.Remove(tile);
    }

    public void PickButton(ObjectButton objBtn)
    {
        if (ClickedButton != null)
        {
            ClickedButton.UnSelect();
        }
        this.ClickedButton = objBtn;
        objBtn.Select();
    }

    public List<TileEntry> GetMapData()
    {
        List<TileEntry> mapDataList = new List<TileEntry>();
        foreach (var tile in tileList)
        {
            // If can be destroyed than it is a map element tile and not a base tile
            // We only want to save the map element tiles
            if (tile.CanBeDestroyed)
            {
                TileEntry tileEntry = new TileEntry()
                {
                    point = tile.MapPosition,
                    type = tile.type,
                };
                mapDataList.Add(tileEntry);
            }
        }
        return mapDataList;
    }

    public void ReloadMap(List<TileEntry> pointList)
    {

        tileList.Where(x=>x.CanBeDestroyed).ToList().ForEach(x => x.RemoveTile()); // RemoveTile only removes those tiles that are canbedestroyed

        pointList.OrderBy(entry => entry.point.y)
                 .ToList()
                 .ForEach(entry => tileList.Where(tile => tile.MapPosition.x == entry.point.x && tile.MapPosition.z == entry.point.z)
                                           .OrderByDescending(tile=>tile.MapPosition.y)
                                           .First()
                                           .placeTileTEST(entry.type));

    }


    public void LoadButton()
    {
        XMLManager.Instance.Load(LoadLevel.ChosenMap);
    }

    public void SaveButton()
    {
        var warningHandler = GameObject.FindObjectOfType<WarningMessageHandler>();
        if (!tileList.Any(x=>x.type == TileTypeEnum.LightTile))
        {
            warningHandler.SetWarningMessage("Unsuccesfull map save: Use at least 1 light tile");
        }
        else if (!tileList.Any(x => x.type == TileTypeEnum.StartPosition))
        {
            warningHandler.SetWarningMessage("Unsuccesfull map save: Start position needed");
        }
        else
        {
            warningHandler.SetWarningMessage("Succesfull map save!", false);
            XMLManager.Instance.Save();
        }
    }

}
