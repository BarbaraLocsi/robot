﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldCreator : MonoBehaviour {

    [SerializeField]
    private GameObject tile;

    //[SerializeField]
    //private int xLenght;

    //[SerializeField]
    //private int zLenght;

    // Use this for initialization
    void Start () {
        //createField();
        fieldTileTileList = new List<GameObject>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private List<GameObject> fieldTileTileList;

    /// <summary>
    /// Destroys all fieldTile GameObject
    /// </summary>
    public void removeField()
    {
        foreach (var item in fieldTileTileList)
        {
            Destroy(item);
        }
        fieldTileTileList.Clear();
    }

    /// <summary>
    /// Creates a (zSize) x (xSize) sized field
    /// </summary>
    public void createField(int zSize , int xSize)
    {
        fieldTileTileList.Clear();
        var gameControllerObj = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();

        
        int zAvg = (int)(zSize / 2.0);
        int xAvg = (int)(xSize / 2.0);
        Vector3 cameraCenter = new Vector3(xAvg, 0, zAvg);
        Camera.main.GetComponent<CameraCenter>().Center(cameraCenter);

        for (int z = 0; z < zSize; z++)
        {
            for (int x = 0; x < xSize; x++)
            {
                
                var fieldTile = Instantiate(tile, new Vector3(x, 0, z), Quaternion.identity);
                fieldTileTileList.Add(fieldTile);

                TileScript tileScript = fieldTile.GetComponent<TileScript>();
                tileScript.Setup(new Point(x, 0, z));
                gameControllerObj.addTileToList(tileScript);
            }
        }
    }
}
