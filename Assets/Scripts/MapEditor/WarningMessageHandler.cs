﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WarningMessageHandler : MonoBehaviour {

    [SerializeField] private GameObject warningMessagePanel;
    [SerializeField] private Button warningMessagePanel_CloseButton;
    [SerializeField] private Text warningMessagePanel_WarningText;

    private Color badColor;
    private Color goodColor;

    // Use this for initialization
    void Start () {
        badColor = new Color(255, 131f / 255f, 131f / 255f, 255);
        goodColor = new Color(131f / 255f, 255, 131f / 255f, 255);

        warningMessagePanel_CloseButton.onClick.AddListener(
                () =>
                {
                    warningMessagePanel.SetActive(false);
                });
    }

    public void SetWarningMessage(string message, bool bad = true)
    {
        warningMessagePanel_WarningText.text = message;
        warningMessagePanel.SetActive(true);

        warningMessagePanel.GetComponent<Image>().color = bad ? badColor : goodColor;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
