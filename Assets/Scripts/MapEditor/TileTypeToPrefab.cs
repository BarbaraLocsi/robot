﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileTypeToPrefab : MonoBehaviour {

    public GameObject Tile;
    public GameObject LightTile;
    public GameObject StartPosition;

    public GameObject getPrefab(TileTypeEnum type)
    {
        switch (type)
        {
            case TileTypeEnum.LightTile: return LightTile;
            case TileTypeEnum.Tile: return Tile;
            case TileTypeEnum.StartPosition: return StartPosition;
            default: return Tile;
        }
    }

    public TileTypeEnum getType(GameObject gameObject)
    {
        if (gameObject == LightTile)
        {
            return TileTypeEnum.LightTile;
        }
        else if (gameObject == Tile)
        {
            return TileTypeEnum.Tile;
        }
        else if (gameObject == StartPosition)
        {
            return TileTypeEnum.StartPosition;
        }
        return TileTypeEnum.Tile;
    }

}
