﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MapEntry  {

    public List<TileEntry> tileList;
    public string mapName;
    public DateTime creationDate;
    public int xSize;
    public int ySize;
}
