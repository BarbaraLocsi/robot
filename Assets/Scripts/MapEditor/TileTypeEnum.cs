﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[System.Serializable]
public enum TileTypeEnum
{
    Tile, LightTile, StartPosition, FieldTile
}