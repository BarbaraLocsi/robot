﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class CommandTrackableEventHandler : DefaultTrackableEventHandler
{
    public bool IsTracked()
    {
        return mTrackableBehaviour.CurrentStatus == TrackableBehaviour.Status.TRACKED ||
               mTrackableBehaviour.CurrentStatus == TrackableBehaviour.Status.DETECTED;
    }




}
