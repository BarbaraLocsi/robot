﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HideButton : MonoBehaviour {

    [SerializeField] private Button button;
    [SerializeField] private GameObject panelToHide;
    [SerializeField] private string activePanelButtonText;
    [SerializeField] private string inActivePanelButtonText;

    void Start () {
        button.onClick.AddListener(changeVisibility);
    }
	
    void changeVisibility()
    {
        if (panelToHide.activeSelf)
        {
            panelToHide.SetActive(false);
            button.GetComponentInChildren<Text>().text = inActivePanelButtonText;
        }
        else
        {
            panelToHide.SetActive(true);
            button.GetComponentInChildren<Text>().text = activePanelButtonText;
        }
        
    }
}
