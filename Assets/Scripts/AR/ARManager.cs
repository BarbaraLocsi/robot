﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ARManager : MonoBehaviour
{

    [SerializeField] private int referencePointSize = 5;

    public Button StartButton;
    public Button StartP1Button;
    public Button ExecuteButton;
    public Button resetButton;
    //public Text UIText;
    public GameObject MainCommandContainer;
    public GameObject P1CommandContainer;
    public GameObject scriptsGO;

    public GameObject MainCommandPanel;
    public GameObject P1CommandPanel;

    private List<string> commandStringList;
    private CommandContainer main;
    private CommandContainer p1;

    private CommandPanelScript MainCommandPanelScript;
    private CommandPanelScript P1CommandPanelScript;

    // Use this for initialization
    void Start()
    {

        StartButton.onClick.AddListener(() => { startButtonPress(0); });
        StartP1Button.onClick.AddListener(() => { startButtonPress(1); });
        ExecuteButton.onClick.AddListener(() => { executePressed(); });

        commandStringList = new List<string>();
        main = MainCommandContainer.GetComponent<CommandContainer>();
        p1 = P1CommandContainer.GetComponent<CommandContainer>();

        MainCommandPanelScript = MainCommandPanel.GetComponent<CommandPanelScript>();
        P1CommandPanelScript = P1CommandPanel.GetComponent<CommandPanelScript>();

        //
        UIHandler ui = GameObject.FindGameObjectWithTag("UI").GetComponent<UIHandler>();
        ui.ModelUIDic = new Dictionary<CommandContainer, GameObject>();
        ui.ModelUIDic.Add(main, MainCommandPanel);
        ui.ModelUIDic.Add(p1, P1CommandPanel);
        //ui.CommandIcons = CommandIcons;
    }
    private void executePressed()
    {
        CommandContainer.Stop = false;
        // Reset buttont bekapcsoljuk, Start button kikapcsoljuk
        resetButton.gameObject.SetActive(true);
        ExecuteButton.gameObject.SetActive(false);

        // TODO: ez nem biztos hogy kell
        resetButton.enabled = true;
        ExecuteButton.enabled = false;

        StartCoroutine(main.Execute());
    }

    private void startButtonPress(int i)
    {
        var startGO = GameObject.FindGameObjectWithTag("Start");

        //var commandList = findRow(startGO.transform);
        fillCommands(i);

        var result = String.Join(", ", commandStringList.ToArray());

        //UIText.text = result;
        var currentContainerPanel = i == 0 ? MainCommandPanelScript : P1CommandPanelScript;
        currentContainerPanel.AddCommands(commandStringList);


    }

    /// <summary>
    /// Fills the commandStringList and CommandContainer main
    /// </summary>
    private void fillCommands(int i)
    {
        var currentContainer = i == 0 ? main : p1;

        commandStringList.Clear();
        currentContainer.RemoveAll();

        List<GameObject> commandGameObjects = GameObject.FindGameObjectsWithTag("Command").ToList();

        var referencePoint = GameObject.FindGameObjectWithTag("Start");
        if (referencePoint.GetComponent<CommandTrackableEventHandler>().IsTracked() == false)
        {
            commandStringList.Add("Ref");
            return;
        }

        // Get all tracked gameObject
        var trackedCommands = commandGameObjects.Where(x => x.GetComponent<CommandTrackableEventHandler>().IsTracked()).ToList();

        while (trackedCommands.Count != 0)
        {
            Transform also = null;
            Transform felso = null;

            // Get the walls
            foreach (Transform sideWall in referencePoint.transform.GetChild(0).transform)
            {
                // 2 = bottom, 3 = top
                if (sideWall.name == "Side2")
                {
                    also = sideWall;
                }
                if (sideWall.name == "Side3")
                {
                    felso = sideWall;
                }
            }

            // Source: https://answers.unity.com/questions/568773/shortest-distance-from-a-point-to-a-vector.html
            
            // TODO: Ray not needed, may have to normalize
            Vector3 alsoForward = also.forward;
            Vector3 felsoForward = felso.forward;

            Vector3 alsoStartingPoint = also.position;
            Vector3 felsoStartingPoint = felso.position;

            Ray alsoRay = new Ray(alsoStartingPoint, alsoForward);
            Ray felsoRay = new Ray(felsoStartingPoint, felsoForward);

            var currentRowOrdered = trackedCommands.Where(x => Vector3.Cross(alsoRay.direction, x.transform.position - alsoRay.origin).magnitude < referencePointSize &&
                                                                Vector3.Cross(felsoRay.direction, x.transform.position - felsoRay.origin).magnitude < referencePointSize)
                                                   .OrderBy(
                // Order by the distance of the 2 points (reference point and the current object position)
                x => Vector3.Distance(referencePoint.transform.position, x.transform.position))
                .ToList();

            currentRowOrdered.ForEach(x => {
                commandStringList.Add(x.name);
                addCommandToContainer(x, currentContainer);
                // Remove already handled commands from the list
                trackedCommands.Remove(x);
            });

            // No need for reference point if there are no more commands
            if (trackedCommands.Count == 0)
            {
                break;
            }

            // New reference point from the remaining commands will be the nearest to the current reference point
            referencePoint = trackedCommands.OrderBy(x => Vector3.Distance(x.transform.position, referencePoint.transform.position)).First();

            // A referencia pontot is ki kell venni a trackCommandsból és hozzáadni a stringhez
            // Remove new reference point from command list
            commandStringList.Add(referencePoint.name);
            addCommandToContainer(referencePoint, currentContainer);
            trackedCommands.Remove(referencePoint);
        }

    }

    /// <summary>
    /// Adds the the apropriate icommand to the container based on the go
    /// </summary>
    /// <param name="go"></param>
    /// <param name="container"></param>
    private void addCommandToContainer(GameObject go, CommandContainer container)
    {
        if (go.name == "Proc")
        {
            container.AddCommand(p1);
        }
        else
        {
            container.AddCommand(go.GetComponent<ICommand>());
        }
    }


    /// <summary>
    /// Command keresés Raycast használatával
    /// Megkeresi a ebben a sorban az összes Commandot raycastal
    /// </summary>
    /// <param name="from"></param>
    /// <returns></returns>
    private List<string> findRow(Transform from)
    {
        List<string> commands = new List<string>();
        //RaycastHit[] hits;
        //TODO maxdistanve beállítása jó értékre vagy kivétel
        //MAGENTA ÉS FEKETE, A RIGHT JÓ
        //Debug.DrawRay(from.position, from.forward* 10000, Color.green, 10000);
        //Debug.DrawRay(from.position, -from.forward * 10000, Color.blue, 10000);
        Debug.DrawRay(from.position, from.right * 10000, Color.black, 10000);
        Debug.DrawRay(from.position, -from.right * 10000, Color.magenta, 10000);
        //Debug.DrawRay(from.position, from.up * 10000, Color.red, 10000);
        //Debug.DrawRay(from.position, -from.up * 10000, Color.yellow, 10000);

        var hits = Physics.RaycastAll(from.position, from.right, 100.0F).OrderBy(x => x.distance).ToList();

        foreach (var hit in hits)
        {
            // Csak akkor számoljuk bele ha Command taget tartalmaz, hogy a pálya pl ne számítson itt találatnak
            // Jelen esetbe a parentje tartalmazza a command taget mivel a commandoknak 4 sideja van és nem akarom a 4 sidenak beadni mind a command taget
            // tehát azok fognak számitani ami command tagesnek a gyereke
            // vagyis a parent parentje kell mert Commandon belül van a BlueFrame és azon belül a sideok....
            var gameObject = hit.transform.gameObject;
            //var parent = gameObject.transform.parent.gameObject.transform.parent.gameObject; // ez jo
            var parent = gameObject.transform.parent.gameObject;
            var parentOfParent = parent.transform.parent.gameObject;
            if (parentOfParent.CompareTag("Command"))
            {
                commands.Add(parentOfParent.name);
            }
        }
        return commands;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
