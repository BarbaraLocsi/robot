﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class MyGravity : MonoBehaviour {

    [SerializeField]
    float gravity = 9.8f;

    private Rigidbody rb;
    private Vector3 gravityVector;
    private CommandTrackableEventHandler trackableHandler;

    void Start () {
        rb = GetComponent<Rigidbody>();
        gravityVector = -transform.up.normalized*9.8f;

        // TODO: máshogy
        
        var mapImageTarget = GameObject.FindGameObjectWithTag("MapImageTarget");
        trackableHandler = mapImageTarget.GetComponent<CommandTrackableEventHandler>();
    }
	
	void FixedUpdate ()
    {
        // TODO: ha egyszer már aktív volt akkor nem is kell többet nézni az istracked-et
        if (trackableHandler.IsTracked())
        {
            rb.velocity = rb.velocity + gravityVector * Time.deltaTime;
        }
    }
}
