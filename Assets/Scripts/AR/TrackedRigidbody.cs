﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackedRigidbody : MonoBehaviour
{

    private Rigidbody rb;
    private CommandTrackableEventHandler trackableHandler;

    private bool tracked;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        var mapImageTarget = GameObject.FindGameObjectWithTag("MapImageTarget");
        trackableHandler = mapImageTarget.GetComponent<CommandTrackableEventHandler>();
        tracked = false;
    }

    public void Update()
    {
        // Turnes the rigidbody.isKinematic false, if the target is tracked
        if (trackableHandler.IsTracked())
        {
            rb.isKinematic = false;
            // If it is tracked now, but was not before then we update the robots original position to the creator value
            if (!tracked)
            {
                this.gameObject.GetComponent<Robot>().ResetOriginalPositionValueToOriginalPositionGO();
            }
            tracked = true;
        }
        else
        {
            rb.isKinematic = true;
            tracked = true;
        }
    }
}
