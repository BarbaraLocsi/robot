﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CommandPanelScript : MonoBehaviour {

    [SerializeField] GameObject Left;
    [SerializeField] GameObject Right;
    [SerializeField] GameObject Forward;
    [SerializeField] GameObject Jump;
    [SerializeField] GameObject Light;
    [SerializeField] GameObject P;
    [SerializeField] GameObject ReferenceError;

    // Use this for initialization
    void Start () {
        //ReferenceError = new GameObject("RefErr");
        //var text = ReferenceError.AddComponent<Text>();
        //text.font = FOnt
        //text.text = "No reference point";
    }



    public void AddCommands(List<string> commands)
    {
        clearCommands();
        foreach (var item in commands)
        {
            AddCommand(item);
        }
    }

    private void clearCommands()
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }
    
    private void AddCommand(string command)
    {
        var gameObject = getCommandUIElement(command);
        Instantiate(gameObject, this.transform);
    }

    private GameObject getCommandUIElement(string command)
    {
        switch (command)
        {
            case "Left": return Left;
            case "Right":return Right;
            case "Forward": return Forward;
            case "Jump": return Jump;
            case "Light": return Light;
            case "Proc": return P;
            case "Ref": return ReferenceError;
            default:
                Debug.Log("ERROR: no command: " + command);
                return null;
        }
    }
}
