﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCenter : MonoBehaviour{

    [SerializeField] private bool smoothMove = false;

    [SerializeField] private float viewPortX = 0.5f;
    [SerializeField] private float viewPortY = 0.5f;

    [SerializeField] private float zoomValue = 0.5f;
    [SerializeField] private float zoomSpeed = 45;

    private float zoomSizeTarget;

    Vector3 groundCamOffset;
    Vector3 camTarget;
    Vector3 camSmoothDampV;

    private bool initialized = false;

    private new Camera camera;

    //private void al()
    //{
    //    var a = camera.WorldToViewportPoint();
    //    camera.ScreenToWorldPoint
    //}

    private Vector3 GetWorldPosAtViewportPoint(float vx, float vy)
    {
        Ray worldRay = camera.ViewportPointToRay(new Vector3(vx, vy, 0));
        Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
        float distanceToGround;
        groundPlane.Raycast(worldRay, out distanceToGround);
        Debug.Log("distance to ground:" + distanceToGround);
        return worldRay.GetPoint(distanceToGround);
    }

    // Use this for initialization
    void Awake () {
        if (!initialized)
        {
            init();
        }
    }

    private void init()
    {
        camera = GetComponent<Camera>();
        Vector3 groundPos = GetWorldPosAtViewportPoint(viewPortX, viewPortY);
        Debug.Log("groundPos: " + groundPos);
        groundCamOffset = camera.transform.position - groundPos;
        camTarget = camera.transform.position;

        zoomSizeTarget = camera.orthographicSize;

        initialized = true;
    }

    public void Center(Vector3 to)
    {
        if (!initialized)
        {
            init();
        }
        camTarget = to + groundCamOffset;
    }

    private void Update()
    {
        // Centerin the camera
        if (smoothMove)
        {
            // Move the camera smoothly to the target position
            camera.transform.position = Vector3.SmoothDamp(
                  camera.transform.position, camTarget, ref camSmoothDampV, 0.5f);
        }
        else
        {
            camera.transform.position = camTarget;
        }

        // Zooming
        if (System.Math.Abs(camera.orthographicSize - zoomSizeTarget) > zoomSizeTarget/ zoomSpeed)
        {
            float absAmount = zoomValue / zoomSpeed;
            float amount = camera.orthographicSize < zoomSizeTarget ?
                                absAmount :
                                -absAmount;
            camera.orthographicSize += amount;
        }
        else
        {
            camera.orthographicSize = zoomSizeTarget;
        }
    }

    public void ZoomIn()
    {
        zoomSizeTarget = zoomSizeTarget - zoomValue;
    }

    public void ZoomOut()
    {
        zoomSizeTarget = zoomSizeTarget + zoomValue;
    }
}

